'use strict';

require.config({

    baseUrl: 'js/libs',

    paths: {
        models: '../models',
        views: '../views',
        data: '../data',
        templates: '../../templates',
        routers: '../routers'
    },


    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});

require(['jquery', 'backbone', 'routers/AppRouter'], function ($, Backbone, AppRouter) {
  var app = new AppRouter();
  Backbone.history.start({pushState: false});
});