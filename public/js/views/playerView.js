define(function (require) {
  var $        = require('jquery'),
      Backbone = require('backbone');

  return Backbone.View.extend({

    el: '<audio id="audioPlayer" controls autoplay/>',

    initialize: function() {
    },

    setSong: function(song){
      this.model = song;
      this.$el.bind('ended', function() {
        song.ended();
      });
      this.render();
    },

    render: function(){
      return this.$el.attr('src', this.model.get('url'));
    }

  });
});