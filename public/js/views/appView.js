define(function (require) {
  var $             = require('jquery'),
      Backbone      = require('backbone'),
      PlayerView    = require('views/playerView'),
      LibraryView   = require('views/libraryView');

  return Backbone.View.extend({

    className: 'app',

    initialize: function(params){
      this.playerView = new PlayerView({model: this.model.get('currentSong')});

      this.model.on('change:currentSong', function(model){
        this.playerView.setSong(model.get('currentSong'));
      }, this);
    },

    render: function(){
      return this.$el.html([
        this.playerView.$el,
        new LibraryView({collection: this.model.get('library')}).render()
      ]);
    }

  });
});

