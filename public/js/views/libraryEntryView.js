define(function (require) {
  var $        = require('jquery'),
      Backbone = require('backbone');

  return Backbone.View.extend({

    tagName: 'tr',

    template: _.template('<td><button class="play">Play</button></td><td>(<%= artist %>)</td><td><%= title %></td>'),

    events: {
      'click button.play': function() {
        this.model.play();
        this.$el.addClass('currentSong');
      }
    },

    render: function(){
      return this.$el.html(this.template(this.model.attributes));
    }

  });
});