define(function (require) {
  var $                = require('jquery'),
      Backbone         = require('backbone'),
      LibraryEntryView = require('views/libraryEntryView');

  return Backbone.View.extend({

    tagName: "table",
    className: "library",

    initialize: function() {
      this.collection.on('play', function() {
        this.$('tr').removeClass('currentSong');
      }, this);
    },

    render: function(){
      this.$el.children().detach();

      return this.$el.html('<th>Library</th>').append(
        this.collection.map(function(song){
          return new LibraryEntryView({model: song}).render();
        })
      );
    }

  });
});