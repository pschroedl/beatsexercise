define(function (require) {
  var $         = require('jquery'),
      Backbone  = require('backbone'),
      Song      = require('models/song');

  return Backbone.Model.extend({
    initialize: function(params){
      this.set('currentSong', new Song());

      params.library.on('play', function(song){
        this.set('currentSong', song);
      }, this);
    }
  });
});
