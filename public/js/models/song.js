define(function (require) {
  var $        = require('jquery'),
      Backbone = require('backbone');

  return Backbone.Model.extend({
    play: function(){
      this.trigger('play', this);
    },

    ended: function(){
      this.trigger('ended', this);
    }

  });
});
