define(function (require) {
  var $        = require('jquery'),
      Backbone = require('backbone'),
      Song     = require('models/song');

  return Backbone.Collection.extend({
    model: Song
  });
});