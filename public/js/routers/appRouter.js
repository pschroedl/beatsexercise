define(function (require) {
  var $          = require('jquery'),
      Backbone   = require('backbone'),
      Songs      = require('models/songs'),
      App        = require('models/app'),
      AppView    = require('views/appView'),
      PlayerView = require('views/playerView'),
      Song       = require('models/song'),
      songData   = require('data/songData');

  return Backbone.Router.extend({
    routes: {
        "" : "index",
        "songs/:title" : "songs"
    },

    index: function() {
      var library = new Songs(songData);
      var app = new App({library: library});
      var appView = new AppView({model: app});

      $('body').append(appView.render());
    },

    songs: function(title) {
      var selection;
      for (var i = 0; i < songData.length; i++){
        if (songData[i].title === title) {
          selection = i;
        }
      }
      var library = new Songs(songData);
      var player = new PlayerView({model: library.at(selection)});

      $('body').append(player.render());
    }
  });

});