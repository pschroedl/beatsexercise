beatsExercise

Task:
an HTML5/Javascript-based music player, with basic playback and "trick mode" functionality (pause/resume, scrub, skip tracks, etc). The UI/UX is completely up to you, but of course it'd be great if you can include a little bit of CSS3 fanciness. Additional bonus points for the use of node, backbone, mustache, jade, angular, or any other libraries/frameworks that interest you.


