var express = require('express');
var path = require('path');
var app = express();
var port = 8000;

app.use(express.logger('short'));

app.use('/mp3s', express.static(path.join(__dirname + '/public/mp3s')));
app.use(express.static(path.join(__dirname + '/public')));

app.listen(port);

console.log('Server running on port %d', port);

